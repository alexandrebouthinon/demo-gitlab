data "docker_network" "kuzzle" {
  name = "demo-gitlab_default"
}

# Start a container
resource "docker_container" "kuzzle" {
  name  = "kuzzle"
  command = ["kuzzle", "start"]
  networks_advanced {
      name = data.docker_network.kuzzle.name
  }

  ports {
      internal = 7512
      external = 7512
      ip = "127.0.0.1"
  }

  env = [
    "kuzzle_services__storageEngine__client__node=http://elasticsearch:9200",
    "kuzzle_services__internalCache__node__host=redis",
    "kuzzle_services__memoryStorage__node__host=redis",
    "NODE_ENV=production"
  ]

  image = docker_image.kuzzle.latest
}

# Find the latest Ubuntu precise image.
resource "docker_image" "kuzzle" {
  name = "mykuzzle:latest"
}