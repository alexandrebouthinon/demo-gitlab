# TP: Packer et Ansible

## Installation

### Packer

* Récupérez le binaire correspondant à votre machine sur [le site de Packer](https://www.packer.io/downloads)
* Une fois téléchargé, copiez le dans `/usr/bin` en le renommant `packer`
* Vérifiez l'installation en testant la commande `packer --version`
* En cas de problème, vérifiez que le dossier `/usr/bin` est bien votre variable `$PATH` : `export PATH=/usr/bin:$PATH`

### Ansible

* Vérifiez que Python est installé sur votre machine ainsi que le package manager `pip` : `sudo apt install python-pip`
* Installez Ansible 2.9.13 avec `pip install --user ansible==2.9.13`
* Installez Molecule parfum Docker avec `pip install --user molecule[docker]`

## Création du projet

### Packer


* Créez un dossier `hosting` dans le projet: `mkdir hosting`
* Créez un dossier `image` dans le dossier `hosting`
* Créez un nouveau fichier `packer.json` dans `hosting/image` et ajoutez-y ce contenu:

```js
{
  "builders":[
    {
      "type": "docker",
      "image": "kuzzleio/kuzzle:2",
      "commit": true,
      "run_command": [ "-d", "-i", "-t", "{{.Image}}", "/bin/bash" ]
    }
  ],
  "provisioners": [
    {
      "type": "ansible",
      "playbook_file": "./playbook.yml"
    }
  ]
}
```

* Créez un fichier `playbook.yml` dans `hosting/image` et laissez le vide pour le moment

### Ansible 

* Dans un dossier `hosting/image/roles`, lancez la commande `molecule init role kuzzle.with_prometheus`
* Vous devriez avoir une arborescence de `role` Ansible initialisé par Molecule:

```
kuzzle.with_prometheus
├── README.md
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── molecule
│   └── default
│       ├── INSTALL.rst
│       ├── converge.yml
│       ├── create.yml
│       ├── destroy.yml
│       ├── molecule.yml
│       └── verify.yml
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```

#### Éditons tout ca ensemble... 


# Documentation

* [Liste des modules Ansible](https://docs.ansible.com/ansible/2.8/modules/list_of_all_modules.html)
* [Builder Docker](https://www.packer.io/docs/builders/docker)
* [Provisioner Ansible Remote](https://www.packer.io/docs/provisioners/ansible#docker)
