# TP Terraform

## Installation

* Récupérez le binaire correspondant à votre machine sur [le site de Terraform](https://www.terraform.io/downloads.html)
* Une fois téléchargé, copiez le dans `/usr/bin` en le renommant `terraform`
* Vérifiez l'installation en testant la commande `terraform version`
* En cas de problème, vérifiez que le dossier `/usr/bin` est bien votre variable `$PATH` : `export PATH=/usr/bin:$PATH`

> Pour les utilisateurs de VSCode, je recommande fortement l'installation de l'extension `Terraform` de `Anton Kulikov`

## Création du projet

* Dans le répertoire `hosting` (celui du TP Packer/Ansible), créez le dossier `terraform`
* Dans ce dossier créez 4 fichiers:
  * `main.tf`
  * `variables.tf`
  * `outputs.tf`
  * `terraform.tfvars` 
* Vous devriez obtenir l'arborescence de fichiers suivante:

```
terraform
├── main.tf
├── outputs.tf
├── terraform.tfvars
└── variables.tf
```

#### Éditons tout ca ensemble... 

# Documentation

* [Syntax Terraform](https://www.terraform.io/docs/configuration/index.html)
* [Provider Docker](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs)