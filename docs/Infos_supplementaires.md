# Liens et infos supplémentaire

## Jour 1: Scalabilité, LoadBalancing, Proxy SSL...

* [Tutoriels Scaleway (en utilisant leurs services)](https://www.scaleway.com/en/docs/)
* [Kubespray: Kubernetes "self-hosted"](https://github.com/kubernetes-sigs/kubespray)
* [Traefik: LoadBalancer et Routeur](https://doc.traefik.io/traefik/)
* [Guide: Nginx Proxy SSL avec Kuzzle](https://blog.kuzzle.io/fr/securiser-kuzzle-avec-ssl-and-nginx)

## Jour 2: Monitoring

* [Plugin Prometheus Kuzzle](https://github.com/kuzzleio/kuzzle-plugin-prometheus)
* [Prometheus et Grafana pour un serveur self-hosted](https://www.scaleway.com/en/docs/configure-prometheus-monitoring-with-grafana/)
* [Prometheus et Grafana pour Kubernetes](https://www.metricfire.com/blog/monitoring-kubernetes-tutorial-using-grafana-and-prometheus/)
* [Logs management dans Kubernetes (EFK)](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes)
* [Log directement depuis un conteneur Docker vers un service externe (parfait pour le PoC Docker-Compose)](https://docs.docker.com/config/containers/logging/configure/)

## Jour 1: CI/CD et Outils DevOps

* [Runner Gitlab sur AWS EC2 Spot](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/)
* [Terraform: Documentation](https://www.terraform.io/docs/configuration/index.html)
* [Terraform: liste des providers officiels](https://www.terraform.io/docs/providers/index.html)
* [Packer: Documentation](https://www.packer.io/docs)
* [Packer/Ansible/Terraform: Création et déploiement d'une image VMWare (Super tuto qui peut vous servir pour le PoC)](https://gmusumeci.medium.com/deploying-vmware-vsphere-virtual-machines-with-packer-terraform-d0211f72b7f5)
* [Terraform dans une CI: bonnes pratiques](https://learn.hashicorp.com/tutorials/terraform/automate-terraform)
* [GitOps](https://www.gitops.tech/)
