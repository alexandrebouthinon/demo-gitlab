<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Kuzzle configuration](#kuzzle-configuration)
  - [Environments](#environments)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Kuzzle configuration
====================

## Environments

* [`local`](./local) - Local configuration files
  * [`elsaticsearch`](./local/elasticsearch) dockerfile used by the docker-compose.dev.yml with custom configuration
  * [`kuzzlerc`](./local/kuzzlerc) config file for kuzzle
  * [`secret.enc.json`](./local/secret.enc.json) file used by the vault. (eg: [kourou](https://docs.kuzzle.io/core/2/guides/essentials/secrets-vault/))

* [`staging`](./staging) - Staging configuration files
  * [`kuzzlerc`](./local/kuzzlerc) config file for kuzzle
  * [`secret.enc.json`](./local/secret.enc.json) file used by the vault. (eg: [kourou](https://docs.kuzzle.io/core/2/guides/essentials/secrets-vault/))

* [`production`](./production) - Production configuration files
  * [`kuzzlerc`](./local/kuzzlerc) config file for kuzzle
  * [`secret.enc.json`](./local/secret.enc.json) file used by the vault. (eg: [kourou](https://docs.kuzzle.io/core/2/guides/essentials/secrets-vault/))
