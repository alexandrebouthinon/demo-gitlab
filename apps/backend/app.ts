import { Backend } from 'kuzzle'

const app = new Backend('backend')

app.start()
  .then(() => {
    app.log.info('Application started and updated')
  })
  .catch(console.error)
